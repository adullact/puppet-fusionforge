# @summary A two-level hierarchy of hashes
# 
#   Key: <section>
#     Section of the configuration file
#   Value: Hash of <name> => <value> variable configurations
#     <name> should be suffixed by _secret if the variable is secret
#     (it is then stored in a non-world-readable file)
#   Example: { 'core' => { 'privkey_secret' => 'swordfish', 'favourite_colour' => 'blue' } }
#
type Fusionforge::Localconfigs = Hash[
  String,
  Hash[
    String,
    String
  ]
]
