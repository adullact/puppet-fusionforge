# fusionforge

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with fusionforge](#setup)
    * [What fusionforge affects](#what-fusionforge-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with fusionforge](#beginning-with-fusionforge)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This module automates the installation and configuration of a
FusionForge instance. It creates the database, configures the Apache
server, deploys configuration variables, and enables required plugins.

## Setup

### What fusionforge affects

Beyond the FusionForge configuration, the module installs a number of
required packages and runs the FusionForge installer scripts (which in
turn apply various configurations: they create a database and a set of
Apache virtualhosts, they integrate the FusionForge database with PAM
authentication, they install various cron jobs, and so on.

### Beginning with fusionforge

The simplest use of the module is as follows:

        class { 'fusionforge':
          database_password     => 'nrstauie',
          database_password_mta => 'toto99',
        }

This will install FusionForge from its upstream Git repository, using
the 6.1 branch.  A different repository URL can be specified using the
$repository parameter, and a different branch/tag name can be
specified with $release.

## Usage

Most of the configuration can be managed using the class
parameters. For instance:

  * $forge_name allows configuring the site name/title;
  * $web_host allows changing the host name to be used (defaults to
    the host's FQDN);
  * $default_country_code, $default_language and $default_timezone allow configuring
    localisation parameters;
  * many $use_* parameters control whether to enable or disable some
    FusionForge features;
  * the $plugins parameters (an array of strings) is used to configure
    the plugins to enable.  It defaults to installing only scmgit, but
    many others are available.

## Limitations

No effort has been made to install multiple instances on the same host
(that would be hard anyway because of the system users integrations).

Installing/enabling plugins is fine, but there is no code in the
module to disable a plugin later on.

## Development

Development of this module is hosted on Adullact's Gitlab instance, at
https://gitlab.adullact.net/adullact/puppet-fusionforge
Contributions are welcome and should keep with the coding standards.

## Release Notes/Contributors

This module is currently (as of 2021-05) at the "minimal working code"
stage. It does work, and it has a testsuite to show it does, but it
can certainly be improved.

This module is developed by Roland Mas roland.mas@gnurandal.com and
sponsored by [Adullact](https://adullact.org)
