# Reference

<!-- DO NOT EDIT: This document was generated by Puppet Strings -->

## Table of Contents

### Classes

* [`fusionforge`](#fusionforge): Installation and configuration of a FusionForge instance

### Data types

* [`Fusionforge::Localconfigs`](#fusionforgelocalconfigs): A two-level hierarchy of hashes

Key: <section>
  Section of the configuration file
Value: Hash of <name> => <value> variable configurations
  <name> should be suffixed by _secret if the variable is secret
  (it is then stored in a non-world-readable file)
Example: { 'core' => { 'privkey_secret' => 'swordfish', 'favourite_colour' => 'blue' } }

## Classes

### `fusionforge`

This class installs a FusionForge instance from the sources fetched
from a given Git repository; it sets up the core of the forge,
including the database, then the plugins.  It can also propagate
configuration items.

#### Examples

##### 

```puppet
class { 'fusionforge':
  forge_name    => "My very own FusionForge instance",
  web_host      => $facts['networking']['fqdn'],
  lists_host    => "lists.${facts['networking']['fqdn']}",
  extra_configs => {
    'scmgit' => {
      'use_ssh'       => 'no',
      'use_smarthttp' => 'yes',
    },
  }
}
```

#### Parameters

The following parameters are available in the `fusionforge` class.

##### `repository`

Data type: `Stdlib::HTTPUrl`

URL of the Git repository from which to fetch the sources

Default value: `'https://scm.fusionforge.org/anonscm/git/fusionforge/fusionforge.git'`

##### `release`

Data type: `String`

Name of the Git tag, branch or revision-id to use

Default value: `'6.1'`

##### `plugins`

Data type: `Array[String]`

List of plugins to install

Default value: `['scmgit']`

##### `extra_configs`

Data type: `Fusionforge::Localconfigs`

Configuration variables to set, in a hash of hashes (by section
and variable name)

Default value: `{ }`

##### `database_password`

Data type: `String`

The password to use for the database

##### `database_password_mta`

Data type: `String`

The password to use for the MTA to connect to the database

##### `database_name`

Data type: `String`

The name of the FusionForge database

Default value: `'fusionforge'`

##### `database_user`

Data type: `String`

The name of the database user

Default value: `'fusionforge'`

##### `forge_name`

Data type: `String`

The title of the FusionForge instance

Default value: `'FusionForge installed by Puppet'`

##### `web_host`

Data type: `Stdlib::Fqdn`

The virtual host to use for the web site

Default value: `$facts['networking']['fqdn']`

##### `data_path`

Data type: `String`

The path where data is stored on the filesystem

Default value: `'/var/lib/fusionforge'`

##### `lists_host`

Data type: `String`

The hostname to use for mailing lists

Default value: `'lists.$core/web_host'`

##### `scm_host`

Data type: `String`

The hostname to use for SCM subsystems (Git and so on)

Default value: `'scm.$core/web_host'`

##### `users_host`

Data type: `String`

The hostname to use for user email forwarding

Default value: `'users.$core/web_host'`

##### `admin_email`

Data type: `String`

The administrator's email address

Default value: `'webmaster@$core/web_host'`

##### `default_country_code`

Data type: `String`

The 2-letter country code to use by default for users

Default value: `'US'`

##### `default_language`

Data type: `String`

The English name of the language to use by default

Default value: `'English'`

##### `default_theme`

Data type: `String`

The name of the theme to use

Default value: `'funky'`

##### `default_timezone`

Data type: `String`

The time zone to use for displaying dates/times

Default value: `'GMT'`

##### `force_login`

Data type: `Boolean`

Whether to require a login for every page

Default value: ``false``

##### `project_registration_restricted`

Data type: `Boolean`

Whether registration of projects is open to anyone with an account or restricted to forge admins

Default value: ``true``

##### `project_auto_approval`

Data type: `Boolean`

Whether submitted projects are automatically approved or require administrative approval

Default value: ``false``

##### `project_auto_approval_user`

Data type: `String`

Which user to use to auto-approve projects

Default value: `'admin'`

##### `use_activity`

Data type: `Boolean`

Whether to track project activities

Default value: ``true``

##### `use_docman`

Data type: `Boolean`

Whether to enable the documentation manager

Default value: ``true``

##### `use_forum`

Data type: `Boolean`

Whether to enable the forums

Default value: ``true``

##### `use_forum_mail_replies`

Data type: `Boolean`

Whether to accept forum replies by email

Default value: ``false``

##### `use_frs`

Data type: `Boolean`

Whether to use the file release system

Default value: ``true``

##### `fti_config`

Data type: `String`

The full-text-indexing configuration to use in PostgreSQL (experts only).

Default value: `'fusionforge'`

##### `use_ftp`

Data type: `Boolean`

Whether to enable the FTP features

Default value: ``true``

##### `use_gateways`

Data type: `Boolean`

Whether to enable the email gateways

Default value: ``true``

##### `use_mail`

Data type: `Boolean`

Whether to enable the email forwarding

Default value: ``true``

##### `use_manual_uploads`

Data type: `Boolean`

Whether to enable manual/SCP uploads for the file release system

Default value: ``true``

##### `use_news`

Data type: `Boolean`

Whether to use the project news/announcement system

Default value: ``true``

##### `use_people`

Data type: `Boolean`

Whether to use the project openings/hiring system

Default value: ``true``

##### `use_pm`

Data type: `Boolean`

Whether to enable the project management system

Default value: ``true``

##### `use_project_vhost`

Data type: `Boolean`

Whether to provide an Apache virtual host per project

Default value: ``true``

##### `use_ratings`

Data type: `Boolean`

Whether to use the rating system

Default value: ``true``

##### `user_registration_restricted`

Data type: `Boolean`

Whether to restrict user registration

Default value: ``false``

##### `user_notification_on_activation`

Data type: `Boolean`

Whether to send a notification to the admin when a user account is activated

Default value: ``false``

##### `use_scm`

Data type: `Boolean`

Whether to enable the SCM subsystem

Default value: ``true``

##### `use_scm_snapshots`

Data type: `Boolean`

Whether to generate daily snapshots of the SCM repositories

Default value: ``true``

##### `use_scm_tarballs`

Data type: `Boolean`

Whether to generate daily tarball backups of the SCM repositories

Default value: ``true``

##### `allow_multiple_scm`

Data type: `Boolean`

Whether to allow several different SCM systems at the same time for a project

Default value: ``false``

##### `use_shell`

Data type: `Boolean`

Whether to create shell accounts for users

Default value: ``true``

##### `use_shell_limited`

Data type: `Boolean`

Whether to restrict the shell accounts

Default value: ``false``

##### `use_snippet`

Data type: `Boolean`

Whether to enable the code snippets system

Default value: ``true``

##### `use_ssl`

Data type: `Boolean`

Whether to use HTTPS

Default value: ``true``

##### `use_survey`

Data type: `Boolean`

Whether to enable the survey system

Default value: ``true``

##### `use_tracker`

Data type: `Boolean`

Whether to enable the tracker system

Default value: ``true``

##### `use_trove`

Data type: `Boolean`

Whether to enable the Trove map/project categorisation system

Default value: ``true``

##### `use_project_tags`

Data type: `Boolean`

Whether to enable project tags

Default value: ``true``

##### `use_project_full_list`

Data type: `Boolean`

Whether to allow displaying a full list of the projects

Default value: ``true``

##### `sitestats_projects_count`

Data type: `String`

Whether to count only visible projects or all projects in the site statistics

Default value: `'visible'`

##### `allow_project_without_template`

Data type: `Boolean`

Whether to allow creating projects from scratch (without using a template project)

Default value: ``true``

##### `user_display_contact_info`

Data type: `Boolean`

Whether to display users' contact information on their profile page

Default value: ``true``

##### `restrict_users_visibility`

Data type: `Boolean`

Whether to display only related users (ie, users sharing a project with you) or all users

Default value: ``false``

##### `forge_homepage_widget`

Data type: `Boolean`

Whether to use the widget system for the homepage

Default value: ``false``

##### `use_quicknav_default`

Data type: `Boolean`

Whether to display an adaptative quick-navigation menu for users

Default value: ``true``

##### `use_home`

Data type: `Boolean`

Whether to display a link to home page

Default value: ``true``

##### `use_my`

Data type: `Boolean`

Whether to display a link to 'My page' for users

Default value: ``true``

##### `check_password_strength`

Data type: `Boolean`

Whether to perform password strength checks

Default value: ``false``

##### `use_object_associations`

Data type: `Boolean`

Whether to allow associating objects such as tracker items

Default value: ``false``

##### `use_tracker_widget_display`

Data type: `Boolean`

Whether to use the widget system for the trackers

Default value: ``false``

##### `use_docman_review`

Data type: `Boolean`

Whether to enable the review system for the document manager

Default value: ``false``

##### `session_expire`

Data type: `Integer`

Duration (in seconds) before an inactive session expires

Default value: `3600`

##### `use_artefacts_dependencies`

Data type: `Boolean`

Whether to allow defining dependencies between tracker items

Default value: ``false``

##### `tracker_parser_type`

Data type: `String`

Parser to use in tracker items (can be 'markdown')

Default value: `'markdown'`

##### `snippet_parser_type`

Data type: `String`

Parser to use in code snippets (can be 'markdown')

Default value: `'markdown'`

##### `use_user_theme`

Data type: `Boolean`

Allow users to pick their theme

Default value: ``true``

##### `scm_single_host`

Data type: `Boolean`

Use a single host for the SCM repositories

Default value: ``true``

##### `system_user`

Data type: `String`

The user that FusionForge should run as

Default value: `'fusionforge'`

##### `system_user_ssh_akc`

Data type: `String`

The user used by the authorized_keys client

Default value: `'fusionforge_ssh_akc'`

##### `apache_auth_realm`

Data type: `String`

The name of the auth realm used for access to the SCM repositories over HTTPS

Default value: `'SCM for FusionForge'`

##### `users_default_gid`

Data type: `Integer`

The default GID for new users

Default value: `100`

##### `default_use_email_forwarding`

Data type: `Boolean`

Whether to default to enabling email forwarding for new users

Default value: ``true``

## Data types

### `Fusionforge::Localconfigs`

A two-level hierarchy of hashes

Key: <section>
  Section of the configuration file
Value: Hash of <name> => <value> variable configurations
  <name> should be suffixed by _secret if the variable is secret
  (it is then stored in a non-world-readable file)
Example: { 'core' => { 'privkey_secret' => 'swordfish', 'favourite_colour' => 'blue' } }

Alias of `Hash[String, Hash[
    String,
    String
  ]]`

