# Changelog

All notable changes to this project will be documented in this file.

## Release 1.1.0

* all dependencies must have a `version_requirement` #9

## Release 1.0.0

Initial release
