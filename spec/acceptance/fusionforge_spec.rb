require 'spec_helper_acceptance'

describe 'fusionforge' do
  context 'with default options' do
    pp = %(
        class { 'fusionforge':
          database_password     => 'nrstauie',
          database_password_mta => 'toto99',
        }
    )

    it 'installs fusionforge and plugins without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'installs fusionforge and plugins idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe port(80) do
      it { is_expected.to be_listening }
    end
    describe port(443) do
      it { is_expected.to be_listening }
    end

    describe command('curl -L http://$(hostname):80') do
      its(:stdout) { is_expected.to match %r{.*FusionForge installed by Puppet.*} }
    end
    describe command('curl -L -k https://$(hostname):443') do
      its(:stdout) { is_expected.to match %r{.*FusionForge installed by Puppet.*} }
    end

    describe file('/etc/fusionforge/config.ini.d/zzzz-secrets.ini') do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 600 }
    end
  end

  context 'with non-default options' do
    pp = %(
        class { 'fusionforge':
          database_password     => 'nrstauie',
          database_password_mta => 'toto99',
          forge_name => "FusionForge reconfigured with 'single quotes'",
        }
    )

    it 'reconfigures fusionforge without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'reconfigures fusionforge idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe port(80) do
      it { is_expected.to be_listening }
    end

    describe command('curl -L http://$(hostname):80') do
      its(:stdout) { is_expected.to match %r{.*FusionForge reconfigured with &apos;single quotes&apos;.*} }
    end
  end
end
