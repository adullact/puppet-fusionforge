require 'spec_helper'

describe 'fusionforge' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          database_password: 'nrstauie',
          database_password_mta: 'toto99',
        }
      end

      it { is_expected.to compile }
    end
  end
end
