require 'beaker-rspec'
require 'beaker-puppet'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper unless ENV['BEAKER_provision'] == 'no'
install_module
install_module_dependencies

RSpec.configure do |c|
  # Configure all nodes in nodeset
  c.before :suite do
    install_module_from_forge('puppetlabs-apt', '>= 6.0.0 < 8.0.0')
    hosts.each do |host|
      pp = %(
        package {'postfix':
          ensure => present,
        }
        class { 'apt': }
        apt::conf { 'workaround-for-ubuntu-bug-1801338':
          priority => 99,
          content  => 'Acquire::http::Pipeline-Depth "0";',
        }
      )

      apply_manifest_on(host, pp, catch_failures: true)
    end
  end
end
