#@summary Installation and configuration of a FusionForge instance
#
#This class installs a FusionForge instance from the sources fetched
#from a given Git repository; it sets up the core of the forge,
#including the database, then the plugins.  It can also propagate
#configuration items.
#
#@example
#  class { 'fusionforge':
#    forge_name    => "My very own FusionForge instance",
#    web_host      => $facts['networking']['fqdn'],
#    lists_host    => "lists.${facts['networking']['fqdn']}",
#    extra_configs => {
#      'scmgit' => {
#        'use_ssh'       => 'no',
#        'use_smarthttp' => 'yes',
#      },
#    }
#  }
#@param repository
#  URL of the Git repository from which to fetch the sources
#@param release
#  Name of the Git tag, branch or revision-id to use
#@param plugins
#  List of plugins to install
#@param extra_configs
#  Configuration variables to set, in a hash of hashes (by section
#  and variable name)
#@param database_password
#  The password to use for the database
#@param database_password_mta
#  The password to use for the MTA to connect to the database
#@param database_name
#  The name of the FusionForge database
#@param database_user
#  The name of the database user
#@param forge_name
#  The title of the FusionForge instance
#@param web_host
#  The virtual host to use for the web site
#@param data_path
#  The path where data is stored on the filesystem
#@param lists_host
#  The hostname to use for mailing lists
#@param scm_host
#  The hostname to use for SCM subsystems (Git and so on)
#@param users_host
#  The hostname to use for user email forwarding
#@param admin_email
#  The administrator's email address
#@param default_country_code
#  The 2-letter country code to use by default for users
#@param default_language
#  The English name of the language to use by default
#@param default_theme
#  The name of the theme to use
#@param default_timezone
#  The time zone to use for displaying dates/times
#@param force_login
#  Whether to require a login for every page
#@param project_registration_restricted
#  Whether registration of projects is open to anyone with an account or restricted to forge admins
#@param project_auto_approval
#  Whether submitted projects are automatically approved or require administrative approval
#@param project_auto_approval_user
#  Which user to use to auto-approve projects
#@param use_activity
#  Whether to track project activities
#@param use_docman
#  Whether to enable the documentation manager
#@param use_forum
#  Whether to enable the forums
#@param use_forum_mail_replies
#  Whether to accept forum replies by email
#@param use_frs
#  Whether to use the file release system
#@param fti_config
#  The full-text-indexing configuration to use in PostgreSQL (experts only).
#@param use_ftp
#  Whether to enable the FTP features
#@param use_gateways
#  Whether to enable the email gateways
#@param use_mail
#  Whether to enable the email forwarding
#@param use_manual_uploads
#  Whether to enable manual/SCP uploads for the file release system
#@param use_news
#  Whether to use the project news/announcement system
#@param use_people
#  Whether to use the project openings/hiring system
#@param use_pm
#  Whether to enable the project management system
#@param use_project_vhost
#  Whether to provide an Apache virtual host per project
#@param use_ratings
#  Whether to use the rating system
#@param user_registration_restricted
#  Whether to restrict user registration
#@param user_notification_on_activation
#  Whether to send a notification to the admin when a user account is activated
#@param use_scm
#  Whether to enable the SCM subsystem
#@param use_scm_snapshots
#  Whether to generate daily snapshots of the SCM repositories
#@param use_scm_tarballs
#  Whether to generate daily tarball backups of the SCM repositories
#@param allow_multiple_scm
#  Whether to allow several different SCM systems at the same time for a project
#@param use_shell
#  Whether to create shell accounts for users
#@param use_shell_limited
#  Whether to restrict the shell accounts
#@param use_snippet
#  Whether to enable the code snippets system
#@param use_ssl
#  Whether to use HTTPS
#@param use_survey
#  Whether to enable the survey system
#@param use_tracker
#  Whether to enable the tracker system
#@param use_trove
#  Whether to enable the Trove map/project categorisation system
#@param use_project_tags
#  Whether to enable project tags
#@param use_project_full_list
#  Whether to allow displaying a full list of the projects
#@param sitestats_projects_count
#  Whether to count only visible projects or all projects in the site statistics
#@param allow_project_without_template
#  Whether to allow creating projects from scratch (without using a template project)
#@param user_display_contact_info
#  Whether to display users' contact information on their profile page
#@param restrict_users_visibility
#  Whether to display only related users (ie, users sharing a project with you) or all users
#@param forge_homepage_widget
#  Whether to use the widget system for the homepage
#@param use_quicknav_default
#  Whether to display an adaptative quick-navigation menu for users
#@param use_home
#  Whether to display a link to home page
#@param use_my
#  Whether to display a link to 'My page' for users
#@param check_password_strength
#  Whether to perform password strength checks
#@param use_object_associations
#  Whether to allow associating objects such as tracker items
#@param use_tracker_widget_display
#  Whether to use the widget system for the trackers
#@param use_docman_review
#  Whether to enable the review system for the document manager
#@param session_expire
#  Duration (in seconds) before an inactive session expires
#@param use_artefacts_dependencies
#  Whether to allow defining dependencies between tracker items
#@param tracker_parser_type
#  Parser to use in tracker items (can be 'markdown')
#@param snippet_parser_type
#  Parser to use in code snippets (can be 'markdown')
#@param use_user_theme
#  Allow users to pick their theme
#@param scm_single_host
#  Use a single host for the SCM repositories
#@param system_user
#  The user that FusionForge should run as
#@param system_user_ssh_akc
#  The user used by the authorized_keys client
#@param apache_auth_realm
#  The name of the auth realm used for access to the SCM repositories over HTTPS
#@param users_default_gid
#  The default GID for new users
#@param default_use_email_forwarding
#  Whether to default to enabling email forwarding for new users
class fusionforge (
  # Standard secret params
  String $database_password,
  String $database_password_mta,
  String $database_name = 'fusionforge',
  String $database_user = 'fusionforge',

  Stdlib::HTTPUrl $repository = 'https://scm.fusionforge.org/anonscm/git/fusionforge/fusionforge.git',
  String $release = '6.1',

  # Standard config params
  String $forge_name = 'FusionForge installed by Puppet',
  Stdlib::Fqdn $web_host = $facts['networking']['fqdn'],
  String $data_path = '/var/lib/fusionforge',
  String $lists_host = 'lists.$core/web_host',
  String $scm_host = 'scm.$core/web_host',
  String $users_host = 'users.$core/web_host',
  String $admin_email = 'webmaster@$core/web_host',
  String $default_country_code = 'US',
  String $default_language = 'English',
  String $default_theme = 'funky',
  String $default_timezone = 'GMT',
  Boolean $force_login = false,
  Boolean $project_registration_restricted = true,
  Boolean $project_auto_approval = false,
  String $project_auto_approval_user = 'admin',
  Boolean $use_activity = true,
  Boolean $use_docman = true,
  Boolean $use_forum = true,
  Boolean $use_forum_mail_replies = false,
  Boolean $use_frs = true,
  String $fti_config = 'fusionforge',
  Boolean $use_ftp = true,
  Boolean $use_gateways = true,
  Boolean $use_mail = true,
  Boolean $use_manual_uploads = true,
  Boolean $use_news = true,
  Boolean $use_people = true,
  Boolean $use_pm = true,
  Boolean $use_project_vhost = true,
  Boolean $use_ratings = true,
  Boolean $user_registration_restricted = false,
  Boolean $user_notification_on_activation = false,
  Boolean $use_scm = true,
  Boolean $use_scm_snapshots = true,
  Boolean $use_scm_tarballs = true,
  Boolean $allow_multiple_scm = false,
  Boolean $use_shell = true,
  Boolean $use_shell_limited = false,
  Boolean $use_snippet = true,
  Boolean $use_ssl = true,
  Boolean $use_survey = true,
  Boolean $use_tracker = true,
  Boolean $use_trove = true,
  Boolean $use_project_tags = true,
  Boolean $use_project_full_list = true,
  String $sitestats_projects_count = 'visible',
  Boolean $allow_project_without_template = true,
  Boolean $user_display_contact_info = true,
  Boolean $restrict_users_visibility = false,
  Boolean $forge_homepage_widget = false,
  Boolean $use_quicknav_default = true,
  Boolean $use_home = true,
  Boolean $use_my = true,
  Boolean $check_password_strength = false,
  Boolean $use_object_associations = false,
  Boolean $use_tracker_widget_display = false,
  Boolean $use_docman_review = false,
  Integer $session_expire = 3600,
  Boolean $use_artefacts_dependencies = false,
  String $tracker_parser_type = 'markdown',
  String $snippet_parser_type = 'markdown',
  Boolean $use_user_theme = true,
  Boolean $scm_single_host = true,
  String $system_user = 'fusionforge',
  String $system_user_ssh_akc = 'fusionforge_ssh_akc',
  String $apache_auth_realm = 'SCM for FusionForge',
  Integer $users_default_gid = 100,
  Boolean $default_use_email_forwarding = true,


  Array[String] $plugins = ['scmgit'],
  Fusionforge::Localconfigs $extra_configs = { },
) {
  $_srcdir = '/usr/src/fusionforge'
  $_source_path = '/usr/local/share/fusionforge'

  $_packages = [
    'make',
    'gettext',
    'php-cli',
    'php-pgsql',
    'php-htmlpurifier',
    'php-http',
    'php-soap',
    'libapache2-mpm-itk',
    'libapache2-mod-svn',
    'libapache2-mod-php',
    'apache2',
    'postgresql-contrib',
    'libnss-pgsql2',
    'unscd',
    'cvs',
    'subversion',
    'viewvc',
    'python-pycurl',
    'libcgi-pm-perl',
    'git',
    'mercurial',
    'bzr',
    'xinetd',
    'python-moinmoin',
    'libapache2-mod-wsgi',
    'python-psycopg2',
    'unoconv',
    'poppler-utils',
    'dpkg-dev',
    'libmarkdown-php',
    'vsftpd',
    'fonts-dejavu-core',
    'mediawiki',
    'locales-all'
  ]

  class { 'postgresql::server':
    service_ensure     => 'running',
    manage_pg_hba_conf => false,
    listen_addresses   => '0.0.0.0',
  }

  vcsrepo { $_srcdir:
    ensure   => present,
    provider => git,
    source   => $repository,
    revision => $release,
  }
  package { $_packages:
    ensure => present,
    before => Exec['install-ff-core'],
  }
  exec { 'install-ff-core':
    cwd         => "${_srcdir}/src",
    command     => 'make all install-base install-shell install-scm',
    path        => '/usr/sbin:/usr/bin:/sbin:/bin',
    refreshonly => true,
    subscribe   => Vcsrepo[$_srcdir],
    notify      => Exec['ff-post-install'],
    require     => Class['postgresql::server'],
  }
  -> exec { 'install-ff-plugins':
    cwd         => "${_srcdir}/src",
    command     => inline_template('make <% @plugins.each do |plugin| -%> install-plugin-<%= plugin %><% end -%>'),
    refreshonly => true,
    path        => '/usr/sbin:/usr/bin:/sbin:/bin',
    notify      => Exec['ff-post-install'],
  }
  file { '/etc/fusionforge':
    ensure => directory,
    mode   => '0755',
  }
  file { '/etc/fusionforge/config.ini.d':
    ensure => directory,
    mode   => '0755',
  }
  file { '/etc/fusionforge/config.ini.d/zzzz-secrets.ini':
    ensure => present,
    mode   => '0600',
  }
  $_standard_configs = {
    'core' => {
      'forge_name'                      => $forge_name,
      'web_host'                        => $web_host,
      'data_path'                       => $data_path,
      'lists_host'                      => $lists_host,
      'scm_host'                        => $scm_host,
      'users_host'                      => $users_host,
      'admin_email'                     => $admin_email,
      'default_country_code'            => $default_country_code,
      'default_language'                => $default_language,
      'default_theme'                   => $default_theme,
      'default_timezone'                => $default_timezone,
      'force_login'                     => $force_login,
      'project_registration_restricted' => $project_registration_restricted,
      'project_auto_approval'           => $project_auto_approval,
      'project_auto_approval_user'      => $project_auto_approval_user,
      'use_activity'                    => $use_activity,
      'use_docman'                      => $use_docman,
      'use_forum'                       => $use_forum,
      'use_forum_mail_replies'          => $use_forum_mail_replies,
      'use_frs'                         => $use_frs,
      'fti_config'                      => $fti_config,
      'use_ftp'                         => $use_ftp,
      'use_gateways'                    => $use_gateways,
      'use_mail'                        => $use_mail,
      'use_manual_uploads'              => $use_manual_uploads,
      'use_news'                        => $use_news,
      'use_people'                      => $use_people,
      'use_pm'                          => $use_pm,
      'use_project_vhost'               => $use_project_vhost,
      'use_ratings'                     => $use_ratings,
      'user_registration_restricted'    => $user_registration_restricted,
      'user_notification_on_activation' => $user_notification_on_activation,
      'use_scm'                         => $use_scm,
      'use_scm_snapshots'               => $use_scm_snapshots,
      'use_scm_tarballs'                => $use_scm_tarballs,
      'allow_multiple_scm'              => $allow_multiple_scm,
      'use_shell'                       => $use_shell,
      'use_shell_limited'               => $use_shell_limited,
      'use_snippet'                     => $use_snippet,
      'use_ssl'                         => $use_ssl,
      'use_survey'                      => $use_survey,
      'use_tracker'                     => $use_tracker,
      'use_trove'                       => $use_trove,
      'use_project_tags'                => $use_project_tags,
      'use_project_full_list'           => $use_project_full_list,
      'sitestats_projects_count'        => $sitestats_projects_count,
      'allow_project_without_template'  => $allow_project_without_template,
      'user_display_contact_info'       => $user_display_contact_info,
      'restrict_users_visibility'       => $restrict_users_visibility,
      'forge_homepage_widget'           => $forge_homepage_widget,
      'use_quicknav_default'            => $use_quicknav_default,
      'use_home'                        => $use_home,
      'use_my'                          => $use_my,
      'check_password_strength'         => $check_password_strength,
      'use_object_associations'         => $use_object_associations,
      'use_tracker_widget_display'      => $use_tracker_widget_display,
      'use_docman_review'               => $use_docman_review,
      'session_expire'                  => $session_expire,
      'use_artefacts_dependencies'      => $use_artefacts_dependencies,
      'tracker_parser_type'             => $tracker_parser_type,
      'snippet_parser_type'             => $snippet_parser_type,
      'use_user_theme'                  => $use_user_theme,
      'scm_single_host'                 => $scm_single_host,
      'system_user'                     => $system_user,
      'system_user_ssh_akc'             => $system_user_ssh_akc,
      'apache_auth_realm'               => $apache_auth_realm,
      'users_default_gid'               => $users_default_gid,
      'default_use_email_forwarding'    => $default_use_email_forwarding,
    }
  }
  $_standard_secrets = {
    'core' => {
      'database_name'         => $database_name,
      'database_user'         => $database_user,
      'database_password'     => $database_password,
      'database_password_mta' => $database_password_mta,
    }
  }

  $_squote_escape = '&apos;'
  $_dquote_escape = '\\"'

  $_standard_configs.each |$_section, $_settings| {
    $_settings.each |$_key, $_value| {
      $_fname = 'zzzz-local.ini'
      $_file = "/etc/fusionforge/config.ini.d/${_fname}"
      augeas { "ff-standard-config-${_section}-${_key}":
        context => "/files/${_file}",
        changes => [
          "set section[.='${_section}'] '${_section}'",
          sprintf("set section[.='%s']/%s \"%s\"",
                  $_section,
                  $_key,
                  regsubst (regsubst(String($_value), '"', $_dquote_escape, 'G'),
                            "'", $_squote_escape, 'G')),
        ],
        lens    => 'IniFile.lns_loose',
        incl    => $_file,
        require => Exec['install-ff-plugins'],
        notify  => Exec['ff-post-install'],
      }
    }
  }
  $_standard_secrets.each |$_section, $_settings| {
    $_settings.each |$_key, $_value| {
      $_fname = 'zzzz-secrets.ini'
      $_file = "/etc/fusionforge/config.ini.d/${_fname}"
      augeas { "ff-standard-secret-${_section}-${_key}":
        context => "/files/${_file}",
        changes => [
          "set section[.='${_section}'] '${_section}'",
          sprintf("set section[.='%s']/%s \"%s\"",
                  $_section,
                  $_key,
                  regsubst (regsubst(String($_value), '"', $_dquote_escape, 'G'),
                            "'", $_squote_escape, 'G')),
        ],
        lens    => 'IniFile.lns_loose',
        incl    => $_file,
        require => [
          Exec['install-ff-plugins'],
          File[$_file],
        ],
        notify  => Exec['ff-post-install'],
      }
    }
  }
  $extra_configs.each |$_section, $_settings| {
    $_settings.each |$_okey, $_value| {
      if ($_okey[-7,7] == '_secret') {
        $_key = $_okey[0,-7]
        $_fname = 'zzzz-secrets.ini'
      } else {
        $_key = $_okey
        $_fname = 'zzzz-local.ini'
      }
      $_file = "/etc/fusionforge/config.ini.d/${_fname}"
      augeas { "ff-local-config-${_section}-${_key}":
        context => "/files${_file}",
        changes => [
          "set section[.='${_section}'] '${_section}'",
          sprintf("set section[.='%s']/%s \"%s\"",
                  $_section,
                  $_key,
                  regsubst (regsubst(String($_value), '"', $_dquote_escape, 'G'),
                            "'", $_squote_escape, 'G')),
        ],
        lens    => 'IniFile.lns_loose',
        incl    => $_file,
        require => Exec['install-ff-plugins'],
        notify  => Exec['ff-post-install'],
      }
    }
  }
  exec { 'ff-post-install':
    cwd         => "${_srcdir}/src",
    command     => 'make post-install',
    environment => [
      'FF_INSTALL_FORCE_HANDLE_SERVICES=yes',
    ],
    refreshonly => true,
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    require     => Service['postgresql'],
    notify      => Service['apache2'],
  }
  service { 'apache2':
    ensure  => running,
    require => Package['apache2'],
  }
  service { 'unscd':
    ensure  => running,
    require => Package['unscd'],
  }
}
